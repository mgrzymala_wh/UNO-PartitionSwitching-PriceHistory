USE [BusinessSemanticLayer]
GO


CREATE OR ALTER PROCEDURE [MRG].[spMerge_REF_DIP_PriceHistory_BSL_OB075]

-- =============================================
-- Author:		WHGROUP\nunderwood
-- Create date: 25/05/2018
-- Description:	Create spMerge_REF_DIP_PriceHistory_BSL_OB075
--				Merges the contents of MRG.PriceHistory table to dbo.PriceHistory table
-- =============================================
-- =============================================
-- Change History
-- Date				User				Change                
-- 25/05/2018		nunderwood          Created
-- 08/06/2018		nunderwood			Removed update of ActionType column to preserve audit operation.
-- 27/06/2018		nunderwood			Added PriceHistoryOrderId (aud_order).
-- 10/07/2018		dhawley				Merges on PriceHistory now as Load has all logic.
-- 11/07/2018		nunderwood			Brought in line with original table structure.
-- 29/06/2020		mgrzymala			https://jira.willhillatlas.com/browse/DANSRE-760 - PriceHistory - split it into Archive, Current, setup view as UNION, daily job to move data
-- =============================================

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    SET XACT_ABORT ON;

    DECLARE @ErrMsg1 NVARCHAR(MAX),
            @ErrMsg2 NVARCHAR(MAX),
            @ErrMsg NVARCHAR(MAX),
            @ErrorProcedure sysname,
            @ErrorNumber INT,
            @ErrorSeverity INT,
            @ErrorState INT,
            @ErrorLine INT,
            @ErrorMessage NVARCHAR(MAX);

    SET @ErrMsg2 = N'';

    DECLARE @MergeAction TABLE
    (
        MergeAction NVARCHAR(10)
    );

    BEGIN TRY
        BEGIN TRANSACTION;
        MERGE [dbo].[PriceHistory_Current] AS Target
        USING [MRG].[PriceHistory] AS Source
        ON Target.PriceHistoryId = Source.PriceHistoryId
        WHEN MATCHED AND EXISTS
        (
                SELECT Source.[PriceHistoryId],
                    Source.[MarketOfferSourceId],
                    Source.[PriceHistoryFromDt],
                    Source.[PriceHistoryToDt],
					Source.[PriceHistoryOrderId],
                    Source.[MarketOfferId],
                    Source.[OddsNumerator],
                    Source.[OddsDenominator],
                    Source.[PriceHistoryStatus],
                    Source.[PriceHistoryDisplayedFlag],
                    Source.[PriceHistoryResult],
                    Source.[PriceHistoryPlace],
                    Source.[CashoutPriceNumerator],
                    Source.[CashoutPriceDenominator],
                    Source.[StartPriceNumerator],
                    Source.[StartPriceDenominator],
					Source.[ActionType]
                EXCEPT
                (SELECT Target.[PriceHistoryId],
                        Target.[MarketOfferSourceId],
                        Target.[PriceHistoryFromDt],
                        Target.[PriceHistoryToDt],
						Target.[PriceHistoryOrderId],
                        Target.[MarketOfferId],
                        Target.[OddsNumerator],
                        Target.[OddsDenominator],
                        Target.[PriceHistoryStatus],
                        Target.[PriceHistoryDisplayedFlag],
                        Target.[PriceHistoryResult],
                        Target.[PriceHistoryPlace],
                        Target.[CashoutPriceNumerator],
                        Target.[CashoutPriceDenominator],
                        Target.[StartPriceNumerator],
                        Target.[StartPriceDenominator],
						Target.[ActionType])
            ) THEN
            UPDATE SET Target.PriceHistoryToDt = Source.PriceHistoryToDt,
					   Target.PriceHistoryOrderId = Source.PriceHistoryOrderId,
                       Target.MarketOfferId = Source.MarketOfferId,
                       Target.OddsNumerator = Source.OddsNumerator,
                       Target.OddsDenominator = Source.OddsDenominator,
                       Target.PriceHistoryStatus = Source.PriceHistoryStatus,
                       Target.PriceHistoryDisplayedFlag = Source.PriceHistoryDisplayedFlag,
                       Target.PriceHistoryResult = Source.PriceHistoryResult,
                       Target.PriceHistoryPlace = Source.PriceHistoryPlace,
                       Target.CashoutPriceNumerator = Source.CashoutPriceNumerator,
                       Target.CashoutPriceDenominator = Source.CashoutPriceDenominator,
                       Target.StartPriceNumerator = Source.StartPriceNumerator,
                       Target.StartPriceDenominator = Source.StartPriceDenominator,
					   Target.ActionType = Source.ActionType,
                       Target.InformationSourceId = Source.InformationSourceId,
                       Target.UpdatedByStaffId = Source.UpdatedByStaffId,
                       Target.SystemUpdatedDt = Source.SystemUpdatedDt
        WHEN NOT MATCHED THEN
            INSERT
            (
                [MarketOfferSourceId],
                [PriceHistoryFromDt],
                [PriceHistoryToDt],
				[PriceHistoryOrderId],
                [MarketOfferId],
                [OddsNumerator],
                [OddsDenominator],
                [PriceHistoryStatus],
                [PriceHistoryDisplayedFlag],
                [PriceHistoryResult],
                [PriceHistoryPlace],
                [CashoutPriceNumerator],
                [CashoutPriceDenominator],
                [StartPriceNumerator],
                [StartPriceDenominator],
				[ActionType],
                [UpdatedByStaffId],
                [InformationSourceId],
                [SystemCreatedDt],
                [SystemUpdatedDt]
            )
            VALUES
            (Source.[MarketOfferSourceId], 
			 Source.[PriceHistoryFromDt],
             Source.[PriceHistoryToDt], 
			 Source.[PriceHistoryOrderId], 
			 Source.[MarketOfferId], 
			 Source.[OddsNumerator], 
			 Source.[OddsDenominator],
             Source.[PriceHistoryStatus], 
			 Source.[PriceHistoryDisplayedFlag], 
			 Source.[PriceHistoryResult],
             Source.[PriceHistoryPlace], 
			 Source.[CashoutPriceNumerator], 
			 Source.[CashoutPriceDenominator],
             Source.[StartPriceNumerator], 
			 Source.[StartPriceDenominator], 
			 Source.[ActionType], 
			 Source.[UpdatedByStaffId],
             Source.[InformationSourceId], 
			 Source.[SystemCreatedDt], 
			 Source.[SystemUpdatedDt])
        OUTPUT $action
        INTO @MergeAction;

        SELECT COUNT_BIG(IIF(MergeAction = 'INSERT', 1, NULL)) AS InsertCount,
               COUNT_BIG(IIF(MergeAction = 'UPDATE', 1, NULL)) AS UpdateCount,
               COUNT_BIG(IIF(MergeAction = 'DELETE', 1, NULL)) AS DeleteCount
        FROM @MergeAction;

        COMMIT TRAN;
    END TRY

    -- Trap Error  
    BEGIN CATCH
        ROLLBACK TRAN;

        SELECT @ErrorProcedure = ERROR_PROCEDURE(),
               @ErrorNumber = ERROR_NUMBER(),
               @ErrorSeverity = ERROR_SEVERITY(),
               @ErrorState = ERROR_STATE(),
               @ErrorLine = ERROR_LINE(),
               @ErrorMessage = ERROR_MESSAGE();

        -- Define Error Message  
        -- Check Data Type of the Error Procedure to cater for extended stored procedures  
        SELECT @ErrMsg1
            = N'Error Procedure: ' + CASE SQL_VARIANT_PROPERTY(@ErrorProcedure, 'BaseType')
                                         WHEN 'varchar' THEN
                                             @ErrorProcedure
                                         ELSE
                                             'unknown'
                                     END + N', Error Number: ' + CAST(@ErrorNumber AS VARCHAR) + N', Error Severity: '
              + CAST(@ErrorSeverity AS VARCHAR) + N', Error State: ' + CAST(@ErrorState AS VARCHAR) + N', Error Line: '
              + CAST(@ErrorLine AS VARCHAR) + N', Error Message: ' + @ErrorMessage;

        /* Optional additional information  
		SET @ErrMsg2 = 'Error executing:' +   
			'SELECT <' + @Param1 + ', sysname, '+ @p1 + '>, <' + @Param2 + ', sys-name, ' + @p2 + '>'  
		*/
        GOTO spError;
    END CATCH;

    -- Return success  
    RETURN 0;

    -- Return Failure  
    spERROR:

    -- Error Message  
    SET @ErrMsg
        = N'Error executing: ' + OBJECT_NAME(@@PROCID) + CHAR(13) + N' - ' + ISNULL(@ErrMsg1, 'Unknown Error') + N' '
          + CHAR(13)

          -- 2012 syntax  
          + IIF(@ErrMsg2 = '', '', ' - Additional Information: ' + @ErrMsg2 + ' ' + CHAR(13)) + N'Server Name: '
          + @@SERVERNAME + N' - Database: ' + DB_NAME() + N', User: ' + SUSER_NAME() + N', SPID: '
          + CAST(@@SPID AS VARCHAR) + N', Now: ' + CAST(GETDATE() AS VARCHAR(121));

    -- Raise Error  
    RAISERROR(   @ErrMsg,
                 @ErrorSeverity,
                 -- Optionally override the error to cause a failure  
                 --,18  
                 @ErrorState
             );

    RETURN -1;
END;