SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		mgrzymala
-- Create date: 2020-04-29
-- Description:	Switch Partition from dbo.PriceHistory to dbo.PriceHistory_Archive - https://jira.willhillatlas.com/browse/DANSRE-495
-- =============================================
CREATE OR ALTER PROCEDURE [dbo].[usp_Get_PartitionBoundaries_NewestNonEmptyPartitionNumber] 
  
  @_HighNonEmptyPartitionBoundary DATETIME OUTPUT
, @_HighPartitionBoundary DATETIME OUTPUT
, @_PartitionNumber INT OUTPUT
, @_TableName NVARCHAR(256)

AS
BEGIN
SELECT 
        @_HighNonEmptyPartitionBoundary = CONVERT(DATETIME, MAX(prv.value))
FROM
            sys.dm_db_partition_stats               AS pst
INNER JOIN  sys.partitions                          AS p    ON pst.partition_id = p.partition_id
INNER JOIN  sys.destination_data_spaces             AS dds  ON pst.partition_number = dds.destination_id
INNER JOIN  sys.data_spaces                         AS ds   ON dds.data_space_id = ds.data_space_id
INNER JOIN  sys.partition_schemes                   AS ps   ON dds.partition_scheme_id = ps.data_space_id
INNER JOIN  sys.partition_functions                 AS pf   ON ps.function_id = pf.function_id
INNER JOIN  sys.indexes                             AS ix   ON pst.object_id = ix.object_id AND pst.index_id = ix.index_id AND dds.partition_scheme_id = ix.data_space_id AND ix.type <= 6 /* Heap or Clustered Index or Columnstore */
LEFT JOIN   sys.partition_range_values              AS prv  ON pf.function_id = prv.function_id AND pst.partition_number = (CASE pf.boundary_value_on_right WHEN 0 THEN prv.boundary_id ELSE (prv.boundary_id+1) END)

WHERE 1 = 1 
AND   (pst.object_id = OBJECT_ID(@_TableName))
AND    pst.row_count > 0
AND    prv.value IS NOT NULL
OPTION	(FORCE ORDER) -- to speed up execution

SELECT 
        @_HighPartitionBoundary = CONVERT(DATETIME, MAX(prv.value))
FROM
            sys.dm_db_partition_stats               AS pst
INNER JOIN  sys.partitions                          AS p    ON pst.partition_id = p.partition_id
INNER JOIN  sys.destination_data_spaces             AS dds  ON pst.partition_number = dds.destination_id
INNER JOIN  sys.data_spaces                         AS ds   ON dds.data_space_id = ds.data_space_id
INNER JOIN  sys.partition_schemes                   AS ps   ON dds.partition_scheme_id = ps.data_space_id
INNER JOIN  sys.partition_functions                 AS pf   ON ps.function_id = pf.function_id
INNER JOIN  sys.indexes                             AS ix   ON pst.object_id = ix.object_id AND pst.index_id = ix.index_id AND dds.partition_scheme_id = ix.data_space_id AND ix.type <= 6 /* Heap or Clustered Index or Columnstore */
LEFT JOIN   sys.partition_range_values              AS prv  ON pf.function_id = prv.function_id AND pst.partition_number = (CASE pf.boundary_value_on_right WHEN 0 THEN prv.boundary_id ELSE (prv.boundary_id+1) END)

WHERE 1 = 1 
AND   (pst.object_id = OBJECT_ID(@_TableName))
--AND    pst.row_count > 0 -- we want the upper boundary regardless of whether or not it is empty or full
AND    prv.value IS NOT NULL
OPTION	(FORCE ORDER) -- to speed up execution

SELECT 
        @_PartitionNumber = MAX(pst.partition_number)
FROM
            sys.dm_db_partition_stats               AS pst
INNER JOIN  sys.partitions                          AS p    ON pst.partition_id = p.partition_id
INNER JOIN  sys.destination_data_spaces             AS dds  ON pst.partition_number = dds.destination_id
INNER JOIN  sys.data_spaces                         AS ds   ON dds.data_space_id = ds.data_space_id
INNER JOIN  sys.partition_schemes                   AS ps   ON dds.partition_scheme_id = ps.data_space_id
INNER JOIN  sys.partition_functions                 AS pf   ON ps.function_id = pf.function_id
INNER JOIN  sys.indexes                             AS ix   ON pst.object_id = ix.object_id AND pst.index_id = ix.index_id AND dds.partition_scheme_id = ix.data_space_id AND ix.type <= 6 /* Heap or Clustered Index or Columnstore */
LEFT JOIN   sys.partition_range_values              AS prv  ON pf.function_id = prv.function_id AND pst.partition_number = (CASE pf.boundary_value_on_right WHEN 0 THEN prv.boundary_id ELSE (prv.boundary_id+1) END)

WHERE 1 = 1 
AND   (pst.object_id = OBJECT_ID(@_TableName))
AND    pst.row_count > 0
AND    prv.value = @_HighNonEmptyPartitionBoundary
OPTION	(FORCE ORDER) -- to speed up execution

END
GO

