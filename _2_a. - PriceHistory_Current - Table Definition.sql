USE [BusinessSemanticLayer]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET XACT_ABORT ON
GO

DROP TABLE IF EXISTS [dbo].[PriceHistory_Current]

CREATE TABLE [dbo].[PriceHistory_Current](
	[PriceHistoryId] [BIGINT] IDENTITY(1,1) NOT NULL,
	[MarketOfferSourceId] [BIGINT] NOT NULL,
	[PriceHistoryFromDt] [DATETIME] NOT NULL,
	[PriceHistoryToDt] [DATETIME] NOT NULL,
	[PriceHistoryOrderId] [BIGINT] NOT NULL,
	[MarketOfferId] [BIGINT] NOT NULL,
	[OddsNumerator] [INT] NULL,
	[OddsDenominator] [INT] NULL,
	[PriceHistoryStatus] [NCHAR](1) NULL,
	[PriceHistoryDisplayedFlag] [NCHAR](1) NULL,
	[PriceHistoryResult] [NCHAR](1) NULL,
	[PriceHistoryPlace] [INT] NULL,
	[CashoutPriceNumerator] [INT] NULL,
	[CashoutPriceDenominator] [INT] NULL,
	[StartPriceNumerator] [INT] NULL,
	[StartPriceDenominator] [INT] NULL,
	[ActionType] [NCHAR](1) NULL,
	[UpdatedByStaffId] [INT] NOT NULL,
	[InformationSourceId] [INT] NOT NULL,
	[SystemCreatedDt] [DATETIME] NOT NULL,
	[SystemUpdatedDt] [DATETIME] NOT NULL) ON [ps_daily_date]([SystemUpdatedDt])
GO

CREATE CLUSTERED INDEX [IX_CL_dbo_PriceHistory_Current_SystemUpdatedDt] ON [dbo].[PriceHistory_Current]
(
	[SystemUpdatedDt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [ps_daily_date]([SystemUpdatedDt])
GO

ALTER TABLE [dbo].[PriceHistory_Current] ADD CONSTRAINT [PK_dbo_PriceHistory_Current_PriceHistoryId]  PRIMARY KEY NONCLUSTERED ([PriceHistoryId], [SystemUpdatedDt])
GO

ALTER TABLE [dbo].[PriceHistory_Current] ADD CONSTRAINT [DF_dbo_PriceHistory_Current_PriceHistoryToDt]  DEFAULT ('9999-12-31 23:59:59') FOR [PriceHistoryToDt]
GO

ALTER TABLE [dbo].[PriceHistory_Current] ADD CONSTRAINT [DF_dbo_PriceHistory_Current_UpdatedByStaffId]  DEFAULT ((1)) FOR [UpdatedByStaffId]
GO

ALTER TABLE [dbo].[PriceHistory_Current] ADD CONSTRAINT [DF_dbo_PriceHistory_Current_InformationSourceId]  DEFAULT ((2)) FOR [InformationSourceId]
GO

DECLARE @SystemUpdatedDtMin DATETIME, @SystemUpdatedDtMax DATETIME, @Sql VARCHAR(1024)

SELECT      @SystemUpdatedDtMin = CONVERT(DATETIME, MIN(sprv.value)),
            @SystemUpdatedDtMax = CONVERT(DATETIME, MAX(sprv.value))
FROM        sys.partition_functions AS spf
INNER JOIN  sys.partition_range_values sprv ON sprv.function_id=spf.function_id
WHERE       (spf.name = N'pf_daily_date')

SELECT @Sql = 'ALTER TABLE [dbo].[PriceHistory_Current] WITH CHECK ADD CONSTRAINT [DF_dbo_PriceHistory_Current_SystemUpdatedDt] CHECK (SystemUpdatedDt >= '''+CONVERT(NVARCHAR(64), @SystemUpdatedDtMin)+''' AND SystemUpdatedDt < '''+CONVERT(NVARCHAR(64), @SystemUpdatedDtMax)+''')'
PRINT 'Processing table: [PriceHistory_Current], adjusting Lowest/Highest-Value-Constraints to: >= '+CONVERT(NVARCHAR(64), @SystemUpdatedDtMin)+' < '+CONVERT(NVARCHAR(64), @SystemUpdatedDtMax)
EXEC(@Sql)

TRUNCATE TABLE [dbo].[PriceHistory_Current]

--SET IDENTITY_INSERT [dbo].[PriceHistory_Current] ON

--BEGIN
--        INSERT INTO [dbo].[PriceHistory_Current]
--        (
--            PriceHistoryId,
--            MarketOfferSourceId,
--            PriceHistoryFromDt,
--            PriceHistoryToDt,
--            PriceHistoryOrderId,
--            MarketOfferId,
--            OddsNumerator,
--            OddsDenominator,
--            PriceHistoryStatus,
--            PriceHistoryDisplayedFlag,
--            PriceHistoryResult,
--            PriceHistoryPlace,
--            CashoutPriceNumerator,
--            CashoutPriceDenominator,
--            StartPriceNumerator,
--            StartPriceDenominator,
--            ActionType,
--            UpdatedByStaffId,
--            InformationSourceId,
--            SystemCreatedDt,
--            SystemUpdatedDt 
--        )
--        SELECT 
--            PriceHistoryId,
--            MarketOfferSourceId,
--            PriceHistoryFromDt,
--            PriceHistoryToDt,
--            PriceHistoryOrderId,
--            MarketOfferId,
--            OddsNumerator,
--            OddsDenominator,
--            PriceHistoryStatus,
--            PriceHistoryDisplayedFlag,
--            PriceHistoryResult,
--            PriceHistoryPlace,
--            CashoutPriceNumerator,
--            CashoutPriceDenominator,
--            StartPriceNumerator,
--            StartPriceDenominator,
--            ActionType,
--            UpdatedByStaffId,
--            InformationSourceId,
--            SystemCreatedDt,
--            SystemUpdatedDt 
--        FROM [dbo].[PriceHistory_Source] WHERE SystemUpdatedDt >= @SystemUpdatedDtMin AND SystemUpdatedDt < @SystemUpdatedDtMax
--END

--SET IDENTITY_INSERT [dbo].[PriceHistory_Current] OFF

--Processing table: [PriceHistory_Current], adjusting Lowest/Highest-Value-Constraints to: >= Jan  1 2013 12:00AM < Jul  1 2013 12:00AM

--(51000000 rows affected)

-- around 10 minutes to complete
