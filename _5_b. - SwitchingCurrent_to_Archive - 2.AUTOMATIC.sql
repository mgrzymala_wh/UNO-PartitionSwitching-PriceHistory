USE [BusinessSemanticLayer]
GO

SET NOCOUNT ON

DECLARE @LowPartitionBoundary DATETIME
DECLARE @HighPartitionBoundary DATETIME
DECLARE @PartitionNumber INT
DECLARE @MinAllowedBoundaryDifference INT = 180
DECLARE @Sql VARCHAR(1024)
DECLARE @StopAtDate DATETIME = CONVERT(DATETIME, (CONVERT(DATE, (DATEADD(DAY, -5, GETDATE())))))
DECLARE @SourceTableName VARCHAR(256) = 'PriceHistory_Current'
DECLARE @ErrMsg VARCHAR(MAX)

EXEC [dbo].[usp_Get_PartitionBoundaries_OldestNonEmptyPartitionNumber] @LowPartitionBoundary OUTPUT, @HighPartitionBoundary OUTPUT, @PartitionNumber OUTPUT, @_TableName = @SourceTableName
EXEC [dbo].[usp_CheckAdjust_PartitionBoundaries] @_LowPartitionBoundary = @LowPartitionBoundary, @_HighPartitionBoundary = @HighPartitionBoundary, @_MinAllowed_BoundaryDifference = @MinAllowedBoundaryDifference

IF (@PartitionNumber IS NULL)
BEGIN
      SET @ErrMsg = 'Error executing: ' + COALESCE(OBJECT_NAME(@@PROCID), 'Unknown procedure') + CHAR(13)
      SET @ErrMsg += CONCAT('OldestNonEmptyPartitionNumber retrieved is NULL; Make sure table: ', @SourceTableName, ' exists, is partitioned and contains data')
      ;THROW 99001, @ErrMsg, 1;
END
ELSE IF (@LowPartitionBoundary >= @StopAtDate)
BEGIN
        PRINT('Oldest non-empty partition of the '+CONVERT(NVARCHAR(256), @SourceTableName)+' table has a begin-timestamp ('+CONVERT(NVARCHAR(32), @LowPartitionBoundary)+') later or equal to @StopAtDate: '+CONVERT(NVARCHAR(32), @StopAtDate)+' - no switching ation needed')
END
ELSE 
WHILE ((@PartitionNumber IS NOT NULL) AND (@LowPartitionBoundary < @StopAtDate))
BEGIN
        EXEC [dbo].[usp_SwitchPartition_PriceHistory_CurrentToArchive] @_LowPartitionBoundary = @LowPartitionBoundary, @_HighPartitionBoundary = @HighPartitionBoundary, @_PartitionNumber = @PartitionNumber
        EXEC [dbo].[usp_Get_PartitionBoundaries_OldestNonEmptyPartitionNumber] @LowPartitionBoundary OUTPUT, @HighPartitionBoundary OUTPUT, @PartitionNumber OUTPUT, @_TableName = @SourceTableName
        EXEC [dbo].[usp_CheckAdjust_PartitionBoundaries] @_LowPartitionBoundary = @LowPartitionBoundary, @_HighPartitionBoundary = @HighPartitionBoundary, @_MinAllowed_BoundaryDifference = @MinAllowedBoundaryDifference
END

