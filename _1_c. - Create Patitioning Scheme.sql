USE [ReplicationDB]
GO

IF EXISTS (SELECT * FROM sys.partition_schemes WHERE [name] = 'ps_daily_date')
BEGIN
    DROP PARTITION SCHEME [ps_daily_date];
END

DECLARE @DatePartitionScheme NVARCHAR(MAX) = 
    N'CREATE PARTITION SCHEME [ps_daily_date] AS PARTITION [pf_daily_date] TO (';

DECLARE @i DATETIME, @pf_date_min DATETIME, @pf_date_max DATETIME, @fg NVARCHAR(256)

SELECT          @pf_date_min = CONVERT(DATETIME, MIN(sprv.value))
                , @pf_date_max = CONVERT(DATETIME, MAX(sprv.value))
FROM            sys.partition_functions AS spf
INNER JOIN      sys.partition_range_values sprv ON sprv.function_id=spf.function_id
WHERE           (spf.name = N'pf_daily_date')

SELECT @i = @pf_date_min
WHILE @i <= @pf_date_max 
    BEGIN
        SET @DatePartitionScheme += CHAR(13)+CHAR(10)
        SELECT @fg = [name] FROM sys.filegroups fg WHERE fg.[name] = LEFT(CONVERT(VARCHAR, @i, 112), 6) -- match YYYYMM from date to filgroup name
        SET @DatePartitionScheme += '''' + CAST(@fg AS NVARCHAR(256)) + '''' +IIF(@i < @pf_date_max, N', ', '');  
        SET @i = DATEADD(DAY, 1, @i);  
    END

SET @DatePartitionScheme += N', '+CHAR(13)+CHAR(10)
-- additional entry in pscheme is because pscheme needs one more entry than there is in pfunction:
SET @DatePartitionScheme += '''' + CAST(@fg AS NVARCHAR(256)) + '''';
SET @DatePartitionScheme += N');';

--PRINT @DatePartitionScheme
EXEC sp_executesql @DatePartitionScheme;  
GO
