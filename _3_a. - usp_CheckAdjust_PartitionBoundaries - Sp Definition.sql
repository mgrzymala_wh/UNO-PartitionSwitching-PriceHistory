USE [BusinessSemanticLayer]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		mgrzymala
-- Create date: 2020-04-29
-- Description:	Switch Partition from dbo.PriceHistory to dbo.PriceHistory_Archive - https://jira.willhillatlas.com/browse/DANSRE-495
-- =============================================
CREATE OR ALTER PROCEDURE [dbo].[usp_CheckAdjust_PartitionBoundaries] 
	@_LowPartitionBoundary DATETIME,
    @_HighPartitionBoundary DATETIME,
	--@_Current_BoundaryDifference INT, -- this is not needed as a parameter, it is assigned within the sp body, remove it.
    @_MinAllowed_BoundaryDifference INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
    SET XACT_ABORT ON;

    DECLARE @_Current_BoundaryDifference INT
    DECLARE @ErrMsg VARCHAR(MAX)

            -- 1. check if the difference betwen Lowest Non-empty and Highest Partition-boundaries is within the allowed range:
    SELECT @_Current_BoundaryDifference = DATEDIFF(DAY, @_LowPartitionBoundary, @_HighPartitionBoundary)
    
    IF (@_Current_BoundaryDifference < @_MinAllowed_BoundaryDifference)
    -- not enough space in the partitioning scheme/function, they need to be extended:
    BEGIN
            DECLARE @NumOfDaysMissing INT = @_MinAllowed_BoundaryDifference - @_Current_BoundaryDifference
            PRINT('The difference ('+CONVERT(NVARCHAR(32), @_Current_BoundaryDifference)+' days) between the Lowest Non-empty partition boundary ('+CONVERT(NVARCHAR(32), @_LowPartitionBoundary)+') 
            and the Highest partition-boundary ('+CONVERT(NVARCHAR(32), @_HighPartitionBoundary)+') is NOT within the min. allowed range: '+CONVERT(NVARCHAR(32), @_MinAllowed_BoundaryDifference))+' days'

            PRINT(CONCAT('Partition function/scheme need to be extended by: ', CONVERT(NVARCHAR(64), @NumOfDaysMissing), ' day(s)'))

            DECLARE @Counter INT = 0

            DECLARE @NewHighBoundary DATETIME = DATEADD(DAY, 1, @_HighPartitionBoundary)
            
            WHILE (@Counter < @NumOfDaysMissing)
            BEGIN
                    DECLARE @fg NVARCHAR(256)
                    DECLARE @Sql VARCHAR(1024)
                    PRINT(CONCAT('Adjusting pf/ps, iteration: ', CONVERT(NVARCHAR(64), @Counter+1), ' of ', CONVERT(NVARCHAR(64), @NumOfDaysMissing)))

                    SELECT @fg = [name] FROM sys.filegroups fg WHERE fg.[name] = LEFT(CONVERT(VARCHAR, @NewHighBoundary, 112), 6) -- match YYYYMM from date to filgroup name
                    --SELECT @NewHighBoundary = CONVERT(DATETIME, '2999-01-01')
                    IF (@fg IS NULL)
                    BEGIN
                            SET @ErrMsg = 'Error executing: ' + COALESCE(OBJECT_NAME(@@PROCID), 'Unknown procedure') + CHAR(13) + '
                            Could not find in sys.filegroups a match for: '+LEFT(CONVERT(VARCHAR, @NewHighBoundary, 112), 6)
                            RAISERROR ( @ErrMsg 
	                                   , 18 -- @ErrorSeverity -- override to 20 to break execution
	                                   ,-1
	                                  ) WITH LOG;
                            --SELECT @fg = 'PRIMARY'
                    END
                    ELSE 
                    BEGIN 
                            SET @Sql = 'ALTER PARTITION SCHEME ps_daily_date NEXT USED ['+@fg+'];'
                            PRINT(CONCAT('Adjusting PARTITION SCHEME ps_daily_date: ', @Sql))
                            EXEC(@Sql)
                            
                            PRINT(CONCAT('Adjusting PARTITION FUNCTION pf_daily_date: ', CONVERT(NVARCHAR(64), @NewHighBoundary)))
                            ALTER PARTITION FUNCTION pf_daily_date () SPLIT RANGE (@NewHighBoundary)
                            SELECT @NewHighBoundary = DATEADD(DAY, 1, @NewHighBoundary)
                            SELECT @Counter = @Counter + 1
                            
                            -- line below would work only of partitions contain no columnstore indexes (partitions containing a columnstore index cannot be merged):
                            --ALTER PARTITION FUNCTION pf_daily_date() MERGE RANGE (DATEDIFF(DAY, 1, @_LowPartitionBoundary));
                    END
            END     
    END
    
    ELSE
    
    BEGIN
        -- enough space in the partitioning scheme, no need to extend anything:
        PRINT('No need to extend partition, the difference ('+CONVERT(NVARCHAR(32), @_Current_BoundaryDifference)+' days) between the Lowest Non-empty partition boundary ('+CONVERT(NVARCHAR(32), @_LowPartitionBoundary)+') 
          and the Highest partition-boundary ('+CONVERT(NVARCHAR(32), @_HighPartitionBoundary)+') is within the min. allowed range: '+CONVERT(NVARCHAR(32), @_MinAllowed_BoundaryDifference))+' days'+ CHAR(13)
    END

END
GO


