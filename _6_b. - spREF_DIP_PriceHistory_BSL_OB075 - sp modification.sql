USE [StagingOpenbet]
GO

/****** Object:  StoredProcedure [src].[spREF_DIP_PriceHistory_BSL_OB075]    Script Date: 6/29/2020 5:38:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE OR ALTER PROCEDURE [src].[spREF_DIP_PriceHistory_BSL_OB075]
    @gv_CurrReqId INT,
    @PKV_InformationSourceId INT

-- ==========================================
-- Author:		WHGROUP\nunderwood
-- Create date: 25/05/2018
-- Description:	Provides a dataset from StagingOpenbet for REF_DIP_PriceHistory_BSL OB075
-- =============================================
-- =============================================
-- Change History
-- Date			User						Change                
-- 25/05/2018	WHGROUP\nunderwood			Initial version
-- 07/06/2018	WHGROUP\nunderwood			Rewrite
-- 21/06/2018	WHGROUP\nunderwood			Extension of business key to include aud_order due to edge cases where price_changed_at and cr_date may be the same in multiple updates
-- 02/07/2018	WHGroup\nunderwood			Progressive changes as a result of data driven testing 24/06 - 05/07/2018.
-- 11/07/2016	WHGroup\nunderwood			Re-written and simplified to address newly discovered data behaviours and maintainability.
-- 17/07/2018	WHGroup\nunderwood			Changed to allow aud_op = 'I' to be interpreted same as aud_op = 'U', allowing for handling of multiple insert records in the source data.
-- 29/06/2020   WHGROUP\mgrzymala           https://jira.willhillatlas.com/browse/DANSRE-760 - PriceHistory - split it into Archive, Current, setup view as UNION, daily job to move data
-- =============================================
AS 

DROP TABLE IF EXISTS #MarketOfferSourceId;
DROP TABLE IF EXISTS #PriceHistory;
DROP TABLE IF EXISTS #newRows;
DROP TABLE IF EXISTS #updatingRows;
DROP TABLE IF EXISTS #updatedRows;

CREATE TABLE #MarketOfferSourceId
(
    ev_oc_id BIGINT,
    PRIMARY KEY CLUSTERED (ev_oc_id)
);

INSERT #MarketOfferSourceId
(
    ev_oc_id
)
SELECT DISTINCT
       ev_oc_id
FROM dbo.tevoc_aud
WHERE RequestId = @gv_CurrReqId;


CREATE TABLE #PriceHistory
(
    rn INT,
    NewRowInTarget BIT,
    WillUpdateRow BIT,
    PriceHistoryId BIGINT,
    ev_oc_id BIGINT,
    aud_order BIGINT,
    aud_op NCHAR(1),
    aud_time DATETIME,
    cr_date DATETIME,
    lp_num INT,
    lp_den INT,
    phstatus NCHAR(1),
    displayed NCHAR(1),
    place INT,
    result NCHAR(1),
    cp_num INT,
    cp_den INT,
    sp_num INT,
    sp_den INT,
    price_changed_at DATETIME
);

INSERT #PriceHistory
(
    rn,
    NewRowInTarget,
    WillUpdateRow,
    PriceHistoryId,
    ev_oc_id,
    aud_order,
    aud_op,
    aud_time,
    cr_date,
    lp_num,
    lp_den,
    phstatus,
    displayed,
    place,
    result,
    cp_num,
    cp_den,
    sp_num,
    sp_den,
    price_changed_at
)
SELECT ROW_NUMBER() OVER (PARTITION BY ev_oc_id ORDER BY aud_order) AS rn,
       IIF(PriceHistoryId <> 0, 1, 0) NewRowInTarget,
       0 AS WillUpdateRow,
       PriceHistoryId,
       ev_oc_id,
       aud_order,
       aud_op,
       aud_time,
       cr_date,
       lp_num,
       lp_den,
       phstatus,
       displayed,
       place,
       result,
       cp_num,
       cp_den,
       sp_num,
       sp_den,
       price_changed_at
FROM
(
    SELECT PriceHistoryId AS PriceHistoryId,
           MarketOfferSourceId AS ev_oc_id,
           PriceHistoryOrderId AS aud_order,
           ActionType AS aud_op,
           IIF(ActionType = 'D', PriceHistoryFromDt, '1900-01-01 00:00:00') AS aud_time,
           '1900-01-01 00:00:00' AS cr_date,
           OddsNumerator AS lp_num,
           OddsDenominator AS lp_den,
           PriceHistoryStatus AS phstatus,
           PriceHistoryDisplayedFlag AS displayed,
           PriceHistoryPlace AS place,
           PriceHistoryResult AS result,
           CashoutPriceNumerator AS cp_num,
           CashoutPriceDenominator AS cp_den,
           StartPriceNumerator AS sp_num,
           StartPriceDenominator AS sp_den,
           PriceHistoryFromDt AS price_changed_at
    --FROM BusinessSemanticLayer.dbo.PriceHistory ph
    FROM BusinessSemanticLayer.dbo.PriceHistory_Current ph
        JOIN #MarketOfferSourceId mos
            ON ph.MarketOfferSourceId = mos.ev_oc_id
    UNION
    SELECT 0 AS PriceHistoryId,
           ev_oc_id,
           aud_order,
           aud_op,
           aud_time,
           cr_date,
           lp_num,
           lp_den,
           [status] AS phstatus,
           displayed,
           place,
           result,
           cp_num,
           cp_den,
           sp_num,
           sp_den,
           price_changed_at
    FROM dbo.tevoc_aud
    WHERE RequestId = @gv_CurrReqId
) a;

CREATE NONCLUSTERED INDEX ix_TmpPriceHistory
ON #PriceHistory (
                     PriceHistoryId
                 )
INCLUDE (
            ev_oc_id,
			aud_order, 
			NewRowInTarget,
            WillUpdateRow	  	
        );

-- get rid of any tevoc_aud entries which have already been processed (because they are in PriceHistory already)
-- and we know this because their aud_order will be less than the highest aud_order for entries with a non-zero 
-- PriceHistoryId
DELETE pha
FROM #PriceHistory pha
    LEFT OUTER JOIN #PriceHistory phb
        ON pha.ev_oc_id = phb.ev_oc_id
WHERE pha.PriceHistoryId = 0
      AND phb.PriceHistoryId <> 0
      AND pha.aud_order <= phb.aud_order;

-- all inserts are new rows
UPDATE #PriceHistory
SET NewRowInTarget = 1
WHERE aud_op IN ( 'D' );

-- all price changes are new rows
CREATE TABLE #newRows
(
    ev_oc_id BIGINT,
    rn BIGINT
);
INSERT #newRows
(
    ev_oc_id,
    rn
)
SELECT ev_oc_id,
       rn
FROM
(
    SELECT rn,
           ev_oc_id,
           lp_num,
           LAG(lp_num) OVER (PARTITION BY ev_oc_id ORDER BY rn ASC) AS last_lp_num,
           lp_den,
           LAG(lp_den) OVER (PARTITION BY ev_oc_id ORDER BY rn ASC) AS last_lp_den,
           cp_num,
           LAG(cp_num) OVER (PARTITION BY ev_oc_id ORDER BY rn ASC) AS last_cp_num,
           cp_den,
           LAG(cp_den) OVER (PARTITION BY ev_oc_id ORDER BY rn ASC) AS last_cp_den
    FROM #PriceHistory
) a
WHERE (ISNULL(lp_num, -1) <> ISNULL(last_lp_num, -1))
      OR (ISNULL(lp_den, -1) <> ISNULL(last_lp_den, -1))
      OR (ISNULL(cp_num, -1) <> ISNULL(last_cp_num, -1))
      OR (ISNULL(cp_den, -1) <> ISNULL(last_cp_den, -1));

UPDATE ph
SET ph.NewRowInTarget = 1,
	ph.price_changed_at = IIF(n.rn > 1 AND price_changed_at IS NULL, aud_time, price_changed_at)
FROM #PriceHistory ph
    INNER JOIN #newRows n
        ON ph.ev_oc_id = n.ev_oc_id
           AND ph.rn = n.rn;

-- the first row in each set is always a new row
UPDATE ph
SET NewRowInTarget = 1
FROM #PriceHistory ph
WHERE rn = 1;

-- each new row gets their non-SCD data updated with the data that is in the last NON-new-Row before the next new row
-- FIND and mark these rows
CREATE TABLE #updatingRows
(
    ev_oc_id BIGINT,
    rn BIGINT
);
INSERT #updatingRows
(
    ev_oc_id,
    rn
)
SELECT ev_oc_id,
       rn
FROM
(
    SELECT rn,
           ev_oc_id,
           NewRowInTarget,
           LEAD(NewRowInTarget) OVER (PARTITION BY ev_oc_id ORDER BY rn) AS next_New_Row
    FROM #PriceHistory
) a
WHERE NewRowInTarget = 0
      AND
      (
          next_New_Row = 1
          OR next_New_Row IS NULL
      );

UPDATE ph
SET WillUpdateRow = 1
FROM #PriceHistory ph
    INNER JOIN #updatingRows n
        ON ph.ev_oc_id = n.ev_oc_id
           AND ph.rn = n.rn;

-- remove all the rows that are not needed since the last updated row 
-- will keep these changes too and we aren't recording intermediate changes.

DELETE FROM #PriceHistory
WHERE NewRowInTarget = 0
      AND WillUpdateRow = 0;

-- return the results: 
-- note: the merge now matches on PriceHistoryId - PriceHistoryFromDt was given a dummy value as it came in from the PriceHistory table but since
--       we never update the PriceHistoryFromDt if there is an existing PriceHistoryId, this is fine.

-- in the temp table the next row is being examined and if it is a NOT a new row (ie. it is a update row), then the previous row is updated with its' values
-- in the non-SCD positions

-- since Deleted (D) rows always are considered to have their price_changed_at at the moment they are recorded, this is given the aud_time if D.

CREATE TABLE #updatedRows
(
    PriceHistoryId BIGINT,
    ev_oc_id BIGINT,
    aud_order BIGINT,
    aud_op NCHAR(1),
    aud_time DATETIME,
    cr_date DATETIME,
    lp_num INT,
    lp_den INT,
    phstatus NCHAR(1),
    displayed NCHAR(1),
    place INT,
    result NCHAR(1),
    cp_num INT,
    cp_den INT,
    sp_num INT,
    sp_den INT,
    price_changed_at DATETIME,
    NewRowInTarget BIT
);
INSERT #updatedRows
(
    PriceHistoryId,
    ev_oc_id,
    aud_order,
    aud_op,
    aud_time,
    cr_date,
    lp_num,
    lp_den,
    phstatus,
    displayed,
    place,
    result,
    cp_num,
    cp_den,
    sp_num,
    sp_den,
    price_changed_at,
    NewRowInTarget
)
SELECT PriceHistoryId,
       ev_oc_id,
       IIF(LEAD(NewRowInTarget) OVER (PARTITION BY ev_oc_id ORDER BY rn) = 0,
           LEAD(aud_order) OVER (PARTITION BY ev_oc_id ORDER BY rn),
           aud_order) AS aud_order,
       aud_op,
       aud_time,
       cr_date,
       lp_num,
       lp_den,
       IIF(LEAD(NewRowInTarget) OVER (PARTITION BY ev_oc_id ORDER BY rn) = 0,
           LEAD(phstatus) OVER (PARTITION BY ev_oc_id ORDER BY rn),
           phstatus) AS phstatus,
       IIF(LEAD(NewRowInTarget) OVER (PARTITION BY ev_oc_id ORDER BY rn) = 0,
           LEAD(displayed) OVER (PARTITION BY ev_oc_id ORDER BY rn),
           displayed) AS displayed,
       IIF(LEAD(NewRowInTarget) OVER (PARTITION BY ev_oc_id ORDER BY rn) = 0,
           LEAD(place) OVER (PARTITION BY ev_oc_id ORDER BY rn),
           place) AS place,
       IIF(LEAD(NewRowInTarget) OVER (PARTITION BY ev_oc_id ORDER BY rn) = 0,
           LEAD(result) OVER (PARTITION BY ev_oc_id ORDER BY rn),
           result) AS result,
       cp_num,
       cp_den,
       IIF(LEAD(NewRowInTarget) OVER (PARTITION BY ev_oc_id ORDER BY rn) = 0,
           LEAD(sp_num) OVER (PARTITION BY ev_oc_id ORDER BY rn),
           sp_num) AS sp_num,
       IIF(LEAD(NewRowInTarget) OVER (PARTITION BY ev_oc_id ORDER BY rn) = 0,
           LEAD(sp_den) OVER (PARTITION BY ev_oc_id ORDER BY rn),
           sp_den) AS sp_den,
       IIF(aud_op = 'D', aud_time, price_changed_at) AS price_changed_at,
       NewRowInTarget
FROM #PriceHistory;

SELECT PriceHistoryId,
       ev_oc_id AS MarketOfferSourceId,
       COALESCE(price_changed_at, cr_date, aud_time) AS PriceHistoryFromDt,
       IIF(aud_op = 'D',
           aud_time,
           IIF(LEAD(ev_oc_id) OVER (PARTITION BY ev_oc_id ORDER BY aud_order) = ev_oc_id,
               LEAD(price_changed_at) OVER (PARTITION BY ev_oc_id ORDER BY aud_order),
               '9999-12-31 23:59:59')) AS PriceHistoryToDt,
       aud_order AS PriceHistoryOrderId,
       mo.MarketOfferId,
       phstatus AS PriceHistoryStatus,
       displayed AS PriceHistoryDisplayedFlag,
       result AS PriceHistoryResult,
       place AS PriceHistoryPlace,
       lp_num AS OddsNumerator,
       lp_den AS OddsDenominator,
       cp_num AS CashoutPriceNumerator,
       cp_den AS CashoutPriceDenominator,
       sp_num AS StartPriceNumerator,
       sp_den AS StartPriceDenominator,
       aud_op AS ActionType,
       1 AS UpdatedByStaffId,
       1 AS InformationSourceId,
       GETDATE() AS SystemCreatedDt,
       GETDATE() AS SystemUpdatedDt
FROM #updatedRows u
    INNER JOIN BusinessSemanticLayer.dbo.MarketOffers mo
        ON u.ev_oc_id = mo.MarketOfferSourceId
WHERE u.NewRowInTarget = 1;

DROP TABLE IF EXISTS #MarketOfferSourceId;
DROP TABLE IF EXISTS #PriceHistory;
DROP TABLE IF EXISTS #newRows;
DROP TABLE IF EXISTS #updatingRows;
DROP TABLE IF EXISTS #updatedRows;


GO


