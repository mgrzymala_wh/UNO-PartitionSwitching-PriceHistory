USE [ReplicationDB]
GO

DROP TABLE IF EXISTS [dbo].[PriceHistory_Archive]
DROP TABLE IF EXISTS [dbo].[PriceHistory_CurrentToArchive_Staging]
DROP TABLE IF EXISTS [dbo].[PriceHistory_Current]
DROP TABLE IF EXISTS [dbo].[PriceHistory_RecycleBin]

IF EXISTS (SELECT * FROM sys.partition_schemes WHERE [name] = 'ps_daily_date')
BEGIN
    DROP PARTITION SCHEME [ps_daily_date];
END

IF EXISTS (SELECT * FROM sys.partition_functions WHERE [name] = 'pf_daily_date')
BEGIN
    DROP PARTITION FUNCTION [pf_daily_date];
END