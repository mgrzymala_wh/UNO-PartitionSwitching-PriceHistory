USE [BusinessSemanticLayer]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE IF EXISTS [dbo].[PriceHistory_Archive]

CREATE TABLE [dbo].[PriceHistory_Archive](
	[PriceHistoryId] [BIGINT] IDENTITY(1,1) NOT NULL,
	[MarketOfferSourceId] [BIGINT] NOT NULL,
	[PriceHistoryFromDt] [DATETIME] NOT NULL,
	[PriceHistoryToDt] [DATETIME] NOT NULL,
	[PriceHistoryOrderId] [BIGINT] NOT NULL,
	[MarketOfferId] [BIGINT] NOT NULL,
	[OddsNumerator] [INT] NULL,
	[OddsDenominator] [INT] NULL,
	[PriceHistoryStatus] [NCHAR](1) NULL,
	[PriceHistoryDisplayedFlag] [NCHAR](1) NULL,
	[PriceHistoryResult] [NCHAR](1) NULL,
	[PriceHistoryPlace] [INT] NULL,
	[CashoutPriceNumerator] [INT] NULL,
	[CashoutPriceDenominator] [INT] NULL,
	[StartPriceNumerator] [INT] NULL,
	[StartPriceDenominator] [INT] NULL,
	[ActionType] [NCHAR](1) NULL,
	[UpdatedByStaffId] [INT] NOT NULL,
	[InformationSourceId] [INT] NOT NULL,
	[SystemCreatedDt] [DATETIME] NOT NULL,
	[SystemUpdatedDt] [DATETIME] NOT NULL) ON [ps_daily_date]([SystemUpdatedDt])
GO

--ALTER TABLE [dbo].[PriceHistory_Archive] ADD CONSTRAINT [PK_dbo_PriceHistory_Archive_PriceHistoryId]  PRIMARY KEY NONCLUSTERED ([PriceHistoryId], [SystemUpdatedDt])
--GO

ALTER TABLE [dbo].[PriceHistory_Archive] ADD CONSTRAINT [DF_dbo_PriceHistory_Archive_PriceHistoryToDt]  DEFAULT ('9999-12-31 23:59:59') FOR [PriceHistoryToDt]
GO

ALTER TABLE [dbo].[PriceHistory_Archive] ADD CONSTRAINT [DF_dbo_PriceHistory_Archive_UpdatedByStaffId]  DEFAULT ((1)) FOR [UpdatedByStaffId]
GO

ALTER TABLE [dbo].[PriceHistory_Archive] ADD CONSTRAINT [DF_dbo_PriceHistory_Archive_InformationSourceId]  DEFAULT ((2)) FOR [InformationSourceId]
GO

DECLARE @SystemUpdatedDtMin DATETIME, @SystemUpdatedDtMax DATETIME, @Sql VARCHAR(1024)

SELECT      @SystemUpdatedDtMin = CONVERT(DATETIME, MIN(sprv.value)),
            @SystemUpdatedDtMax = CONVERT(DATETIME, MAX(sprv.value))
FROM        sys.partition_functions AS spf
INNER JOIN  sys.partition_range_values sprv ON sprv.function_id=spf.function_id
WHERE       (spf.name = N'pf_daily_date')

--SELECT @Sql = 'ALTER TABLE [dbo].[PriceHistory_Archive] WITH CHECK ADD CONSTRAINT [DF_dbo_PriceHistory_Archive_SystemUpdatedDt] CHECK (SystemUpdatedDt >= '''+CONVERT(NVARCHAR(64), @SystemUpdatedDtMin)+''' AND SystemUpdatedDt < '''+CONVERT(NVARCHAR(64), @SystemUpdatedDtMax)+''')'
SELECT @Sql = 'ALTER TABLE [dbo].[PriceHistory_Archive] WITH CHECK ADD CONSTRAINT [DF_dbo_PriceHistory_Archive_SystemUpdatedDt] CHECK (SystemUpdatedDt < '''+CONVERT(NVARCHAR(64), @SystemUpdatedDtMin)+''')'
PRINT 'Processing table: [PriceHistory_Archive], adjusting Lowest/Highest-Value-Constraints to: < '+CONVERT(NVARCHAR(64), @SystemUpdatedDtMax)
EXEC(@Sql)

CREATE CLUSTERED COLUMNSTORE INDEX [IX_C_Col_dbo_PriceHistory_Archive] ON dbo.[PriceHistory_Archive] WITH (DROP_EXISTING = OFF, COMPRESSION_DELAY = 0, DATA_COMPRESSION = COLUMNSTORE_ARCHIVE)

--SET IDENTITY_INSERT [dbo].[PriceHistory_Archive] ON

--BEGIN
--        INSERT INTO [dbo].[PriceHistory_Archive]
--        (
--            PriceHistoryId,
--            MarketOfferSourceId,
--            PriceHistoryFromDt,
--            PriceHistoryToDt,
--            PriceHistoryOrderId,
--            MarketOfferId,
--            OddsNumerator,
--            OddsDenominator,
--            PriceHistoryStatus,
--            PriceHistoryDisplayedFlag,
--            PriceHistoryResult,
--            PriceHistoryPlace,
--            CashoutPriceNumerator,
--            CashoutPriceDenominator,
--            StartPriceNumerator,
--            StartPriceDenominator,
--            ActionType,
--            UpdatedByStaffId,
--            InformationSourceId,
--            SystemCreatedDt,
--            SystemUpdatedDt 
--        )
--        SELECT 
--            PriceHistoryId,
--            MarketOfferSourceId,
--            PriceHistoryFromDt,
--            PriceHistoryToDt,
--            PriceHistoryOrderId,
--            MarketOfferId,
--            OddsNumerator,
--            OddsDenominator,
--            PriceHistoryStatus,
--            PriceHistoryDisplayedFlag,
--            PriceHistoryResult,
--            PriceHistoryPlace,
--            CashoutPriceNumerator,
--            CashoutPriceDenominator,
--            StartPriceNumerator,
--            StartPriceDenominator,
--            ActionType,
--            UpdatedByStaffId,
--            InformationSourceId,
--            SystemCreatedDt,
--            SystemUpdatedDt 
--        FROM [dbo].[PriceHistory_Source] WHERE SystemUpdatedDt >= DATEADD(DAY, -180, @SystemUpdatedDtMin) AND SystemUpdatedDt < @SystemUpdatedDtMin
--END

--SET IDENTITY_INSERT [dbo].[PriceHistory_Archive] OFF
--GO

TRUNCATE TABLE [dbo].[PriceHistory_Archive]