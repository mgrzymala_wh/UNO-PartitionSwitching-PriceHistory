SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET XACT_ABORT ON
GO

USE [DMOperations]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ArchiveData_Migration_Tracking]') AND type IN (N'U'))
DROP TABLE [dbo].[ArchiveData_Migration_Tracking]
GO

CREATE TABLE [dbo].[ArchiveData_Migration_Tracking](
    [Id]                             INT IDENTITY NOT NULL PRIMARY KEY,
	[SourceTableName]                NVARCHAR(256) NULL,
    [TargetTableName]                NVARCHAR(256) NULL,
    [TargetPartitionNumber]          INT NULL,
	[SourceRows]                     BIGINT NOT NULL,
	[TargetRows]                     BIGINT NOT NULL DEFAULT 0,
	[TargetUsedMB]                   NUMERIC(27, 6) NULL,
	[TargetLowerPartitionBoundary]   DATETIME NULL,
	[TargetUpperPartitionBoundary]   DATETIME NULL,
    [migrated]                       BIT NOT NULL DEFAULT 0,
    [Duration]                       DATETIME NULL
) ON [PRIMARY]
GO

USE [BusinessSemanticLayer]
GO

DECLARE @ArchiveTableName NVARCHAR(256) = 'PriceHistory_Archive'
DECLARE @ArchiveStartingPoint DATETIME = '2020-01-01 00:00:00'

; WITH cte AS (
SELECT 
 	  st.name AS [TableName]  
    , ISNULL(QUOTENAME(ix.name),'Heap') as IndexName 
	, ix.type_desc as [type]
	, prt.partition_number
	, prt.data_compression_desc
	, ps.name as PartitionScheme
	, pf.name as PartitionFunction
	, fg.name as FilegroupName
	, case when ix.index_id < 2 then prt.rows else 0 END as Rows
	, au.TotalMB
	, au.UsedMB
	, c.name AS [ColumnName]
	, CASE WHEN pf.boundary_value_on_right = 1 THEN 'less than' WHEN pf.boundary_value_on_right IS NULL THEN '' ELSE 'less than or equal to' END AS Comparison
    , CASE WHEN pf.boundary_value_on_right = 1 THEN ISNULL(LAG(rv.value) OVER(PARTITION BY pst.object_id ORDER BY pst.object_id, pst.partition_number), CAST('1753-1-1' AS DATETIME))
    
    ELSE NULL END AS LowerPartitionBoundary
	, rv.value
    , prt.data_compression_desc                    AS [DataCompression]

FROM 
	sys.partitions prt
	INNER JOIN  sys.indexes ix                      ON ix.object_id = prt.object_id AND ix.index_id = prt.index_id                                          
	INNER JOIN  sys.tables st                       ON prt.object_id = st.object_id                                                                         
	INNER JOIN  sys.index_columns ic                ON (ic.partition_ordinal > 0 AND ic.index_id = ix.index_id AND ic.object_id = st.object_id)             
	INNER JOIN  sys.columns c                       ON (c.object_id = ic.object_id AND c.column_id = ic.column_id)                                          
	INNER JOIN  sys.data_spaces ds                  ON ds.data_space_id = ix.data_space_id                                                                  
	LEFT JOIN   sys.partition_schemes ps            ON ps.data_space_id = ix.data_space_id                                                                  
	LEFT JOIN   sys.partition_functions pf          ON pf.function_id = ps.function_id                                                                      
	LEFT JOIN   sys.partition_range_values rv       ON rv.function_id = pf.function_id AND rv.boundary_id = prt.partition_number                            
	LEFT JOIN   sys.destination_data_spaces dds     ON dds.partition_scheme_id = ps.data_space_id AND dds.destination_id = prt.partition_number             
	LEFT JOIN   sys.filegroups fg                   ON fg.data_space_id = ISNULL(dds.data_space_id,ix.data_space_id)                                        
	INNER JOIN (
					SELECT 
						         --STR(sum(total_pages)*8./1024, 10,2) as [TotalMB]
                                 --,STR(sum(used_pages)*8./1024, 10,2) as [UsedMB]
                                 SUM(total_pages)*8./1024 as [TotalMB]
                                ,SUM(used_pages)*8./1024 as [UsedMB]
						        ,container_id
					FROM        sys.allocation_units
					GROUP BY    container_id
				)   au
				ON  au.container_id = prt.partition_id
    INNER JOIN sys.dm_db_partition_stats pst            ON pst.partition_id = prt.partition_id

WHERE --au.UsedMB > 0
st.name = @ArchiveTableName --'PriceHistory_Archive' 
)
INSERT INTO [DMOperations].[dbo].[ArchiveData_Migration_Tracking] (
             [TargetPartitionNumber]       
            ,[SourceRows]                  
            --,[SourceUsedMB]                
            ,[TargetLowerPartitionBoundary]
            ,[TargetUpperPartitionBoundary]
            ,[migrated]                         
)
SELECT 
        cte.partition_number
        ,cte.Rows
        --,cte.UsedMB
        ,CONVERT(DATETIME, cte.LowerPartitionBoundary)
        ,CONVERT(DATETIME, cte.value)
        ,0
FROM    cte 
WHERE
        CONVERT(DATETIME, cte.LowerPartitionBoundary) >= @ArchiveStartingPoint

ORDER BY cte.TableName DESC, cte.partition_number
OPTION	(FORCE ORDER) -- to speed up execution

SELECT * FROM [DMOperations].[dbo].[ArchiveData_Migration_Tracking] ORDER BY [TargetPartitionNumber]

