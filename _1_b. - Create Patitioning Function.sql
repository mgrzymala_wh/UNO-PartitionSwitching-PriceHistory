USE [ReplicationDB]
GO

SET NOCOUNT ON
SET XACT_ABORT ON

IF EXISTS (SELECT * FROM sys.partition_functions WHERE [name] = 'pf_daily_date')
BEGIN
    DROP PARTITION FUNCTION [pf_daily_date];
END

DECLARE @StartingDate DATETIME2 = '2018-06-01 00:00:00.000'
DECLARE @DatePartitionFunction NVARCHAR(MAX) = 
    N'CREATE PARTITION FUNCTION [pf_daily_date] (DATETIME) 
    AS RANGE RIGHT FOR VALUES (';  

DECLARE @i DATETIME2 = @StartingDate;
WHILE @i < DATEADD(MONTH, 6, @StartingDate)  -- we add 6 months because that is the most that NVARCHAR(MAX) will accept
    BEGIN  
        SET @DatePartitionFunction += '''' + CAST(@i AS NVARCHAR(10)) + '''' + N', ';  
        SET @i = DATEADD(DAY, 1, @i);  
    END  
SET @DatePartitionFunction += '''' + CAST(@i AS NVARCHAR(10))+ '''' + N');';
PRINT('Creating [pf_daily_date] starting from: '+CONVERT(NVARCHAR(19), @StartingDate)+' ending on: '+CONVERT(NVARCHAR(19), @i))
EXEC sp_executesql @DatePartitionFunction;  

DECLARE @UpperLimit DATETIME
SET @i = DATEADD(DAY, 1, @i);
WHILE (@i <= DATEADD(MONTH, 6, GETDATE())) -- repeat adding 6-month intervals until @i advances to 6 months from now
BEGIN
    SET @StartingDate = @i;
    SET @DatePartitionFunction = '';
    
    SELECT @UpperLimit = MIN(UpperLimit) FROM (VALUES (DATEADD(MONTH, 6, GETDATE())), (DATEADD(MONTH, 6, @StartingDate))) AS v (UpperLimit)
    PRINT('Adding daily intervals to [pf_daily_date] starting from: '+CONVERT(NVARCHAR(19), @i)+' ending on: '+CONVERT(NVARCHAR(19), CONVERT(DATETIME, DATEDIFF(DAY, 0, @UpperLimit))))
    
    -- we split partitions until we reach earlier of 2 values: DATEADD(MONTH, 6, @StartingDate) or DATEADD(MONTH, 6, GETDATE())
    -- this way no matter what the @StartingDate value our [pf_daily_date] will be partitioned until 6 months from now
    WHILE (@i < @UpperLimit) 
    BEGIN
            BEGIN
                SELECT @DatePartitionFunction = 'ALTER PARTITION FUNCTION pf_daily_date () SPLIT RANGE ('''+CONVERT(NVARCHAR(19), @i)+''');'+ CHAR(13);
                --PRINT(@DatePartitionFunction)
                EXEC sp_executesql @DatePartitionFunction;
            END
    SET @i = DATEADD(DAY, 1, @i);
    END
END
GO