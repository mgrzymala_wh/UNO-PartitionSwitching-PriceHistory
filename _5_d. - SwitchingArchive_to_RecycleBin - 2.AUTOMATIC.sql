USE [BusinessSemanticLayer]
GO

SET XACT_ABORT ON
SET NOCOUNT ON

DECLARE @_LowestNonEmptyPartitionBoundary DATETIME
DECLARE @_HighestNonEmptyPartitionBoundary DATETIME
DECLARE @_HighPartitionBoundary DATETIME
DECLARE @_HighestNonEmptyPartitionNumber INT
DECLARE @_LowestNonEmptyPartitionNumber INT
DECLARE @_MaxRecyclingAge INT = 80
DECLARE @Sql VARCHAR(1024), @ErrMsg VARCHAR(MAX)
DECLARE @TableName NVARCHAR(256) = 'PriceHistory_Archive'
DECLARE @_Count BIGINT

EXEC [dbo].[usp_GetCountFromSource]
                  @SourceTableName  = @TableName --'PriceHistory_Source'
                , @FromDate         = NULL
                , @ToDate           = NULL
                , @Count            = @_Count OUTPUT
-- 0. if COUNT(*) FROM @TableName = 0 RAISERROR otherwise get oldest non-empty partition number:
IF (@_Count > 0)
BEGIN
    EXEC [dbo].[usp_Get_PartitionBoundaries_OldestNonEmptyPartitionNumber] @_LowestNonEmptyPartitionBoundary OUTPUT, @_HighPartitionBoundary OUTPUT, @_LowestNonEmptyPartitionNumber OUTPUT, @_TableName = @TableName
    EXEC [dbo].[usp_Get_PartitionBoundaries_NewestNonEmptyPartitionNumber] @_HighestNonEmptyPartitionBoundary OUTPUT, @_HighPartitionBoundary OUTPUT, @_HighestNonEmptyPartitionNumber OUTPUT, @_TableName = @TableName
END
ELSE
BEGIN
      SET @ErrMsg = 'Error executing: ' + COALESCE(OBJECT_NAME(@@PROCID), 'Unknown procedure') + CHAR(13)
      SET @ErrMsg += CONCAT('Table: ', @TableName, ' is empty')
      ;THROW 99001, @ErrMsg, 1;
END

SELECT    @_LowestNonEmptyPartitionNumber                        AS [LowestNonEmptyPartitionNumber]
        , @_HighestNonEmptyPartitionNumber                       AS [HighestNonEmptyPartitionNumber]
        , (@_HighestNonEmptyPartitionNumber - @_MaxRecyclingAge) AS [HighestNonEmptyPartitionNumber - MaxRecyclingAge]

-- 1. check if the difference betwen Lowest Non-empty and Highest Non-empty Partition-boundaries is within the max. allowed range (and if it's not raise an error):
--SELECT @_Boundaries_DayDifference = DATEDIFF(DAY, @_LowestNonEmptyPartitionBoundary, @_HighestNonEmptyPartitionBoundary)
IF (@_LowestNonEmptyPartitionNumber IS NULL OR @_HighestNonEmptyPartitionNumber IS NULL)
BEGIN
      SET @ErrMsg = 'Error executing: ' + COALESCE(OBJECT_NAME(@@PROCID), 'Unknown procedure') + CHAR(13)
      SET @ErrMsg += CONCAT('OldestNonEmptyPartitionNumber or @_Boundaries_DayDifference is NULL, make sure that table: ', @TableName
                        , ' contains values in the date range from: '+CONVERT(NVARCHAR(32), @_LowestNonEmptyPartitionBoundary)+' to: '+CONVERT(NVARCHAR(32), @_HighestNonEmptyPartitionBoundary))
      ;THROW 99001, @ErrMsg, 1;
END

IF (@_LowestNonEmptyPartitionNumber >= (@_HighestNonEmptyPartitionNumber - @_MaxRecyclingAge))
BEGIN
    PRINT('The difference ('+CONVERT(NVARCHAR(32), DATEDIFF(DAY, @_LowestNonEmptyPartitionBoundary, @_HighestNonEmptyPartitionBoundary))+' days) betwen the Lowest Non-empty partition boundary ('+CONVERT(NVARCHAR(32), @_LowestNonEmptyPartitionBoundary)+') 
      and the Highest Non-empty partition boundary ('+CONVERT(NVARCHAR(32), @_HighestNonEmptyPartitionBoundary)+') is within the the max. allowed recycling age: '+CONVERT(NVARCHAR(32), @_MaxRecyclingAge))+' days'+ CHAR(13)+'
      Next Partition to be processed: '+CONVERT(VARCHAR(16), @_LowestNonEmptyPartitionNumber)
END
ELSE
BEGIN
DECLARE @Counter INT = 1
DECLARE @LoopSize INT = ((@_HighestNonEmptyPartitionNumber - @_MaxRecyclingAge) - @_LowestNonEmptyPartitionNumber)
WHILE (@_LowestNonEmptyPartitionNumber < (@_HighestNonEmptyPartitionNumber - @_MaxRecyclingAge))
    BEGIN
        
        PRINT('The difference ('+CONVERT(NVARCHAR(32), DATEDIFF(DAY, @_LowestNonEmptyPartitionBoundary, @_HighestNonEmptyPartitionBoundary))+' days) betwen the Lowest Non-empty partition boundary ('+CONVERT(NVARCHAR(32), @_LowestNonEmptyPartitionBoundary)+') 
          and the Highest Non-empty partition boundary ('+CONVERT(NVARCHAR(32), @_HighestNonEmptyPartitionBoundary)+') is NOT within the the max. allowed recycling age: '+CONVERT(NVARCHAR(32), @_MaxRecyclingAge))+' days'+ CHAR(13)+'
          Needs to switch '+CONVERT(VARCHAR(16), (@_HighestNonEmptyPartitionNumber - @_MaxRecyclingAge) - @_LowestNonEmptyPartitionNumber)+' partitions'
    
            -- 2. SWITCH PARTITION from [PriceHistory_Current] to [PriceHistory_CurrentToArchive_Staging]
        DECLARE @PartitionNumber VARCHAR(16) = CONVERT(VARCHAR(16), @_LowestNonEmptyPartitionNumber)
    
        SELECT @Sql = 'ALTER TABLE dbo.[PriceHistory_Archive] SWITCH PARTITION '+@PartitionNumber+' TO dbo.[PriceHistory_RecycleBin] PARTITION '+@PartitionNumber
        PRINT 'Switching partition: '+@PartitionNumber+' from [PriceHistory_Archive] to [PriceHistory_RecycleBin]; loop: '+CONVERT(VARCHAR(16), @Counter)+' of '+CONVERT(VARCHAR(16), @LoopSize)
        PRINT(@Sql)
        EXEC(@Sql)
        
        EXEC [dbo].[usp_Get_PartitionBoundaries_OldestNonEmptyPartitionNumber] @_LowestNonEmptyPartitionBoundary OUTPUT, @_HighPartitionBoundary OUTPUT, @_LowestNonEmptyPartitionNumber OUTPUT, @_TableName = @TableName
        EXEC [dbo].[usp_Get_PartitionBoundaries_NewestNonEmptyPartitionNumber] @_HighestNonEmptyPartitionBoundary OUTPUT, @_HighPartitionBoundary OUTPUT, @_HighestNonEmptyPartitionNumber OUTPUT, @_TableName = @TableName
        SET @Counter = @Counter + 1
        TRUNCATE TABLE [PriceHistory_RecycleBin]
    END
END

