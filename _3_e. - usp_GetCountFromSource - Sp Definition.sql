SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Marek Grzymala
-- Create date: 2020-06-05T14:42
-- Description:	helper sp to insert into PriceHistory_Archive tables
-- =============================================
ALTER   PROCEDURE [dbo].[usp_GetCountFromSource] 
      @SourceSchemaName NVARCHAR(256)
    , @SourceTableName NVARCHAR(256)
    , @FromDate DATETIME = NULL
    , @ToDate DATETIME = NULL
    , @Count BIGINT OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    SET XACT_ABORT ON;

--------------------------------------

DECLARE @SourceSchemaNameQt NVARCHAR(256) = QUOTENAME(@SourceSchemaName)
DECLARE @SourceTableNameQt NVARCHAR(256) = QUOTENAME(@SourceTableName)
DECLARE @Sql NVARCHAR(MAX)
DECLARE @Parameter NVARCHAR(500);

IF (@FromDate IS NULL OR @ToDate IS NULL)
BEGIN
    SET @Sql = N'SELECT @_count = COUNT_BIG(*) FROM '+@SourceSchemaNameQt+'.'+@SourceTableNameQt+';'
END
ELSE
BEGIN
    PRINT(CONCAT('Getting count from table: ', @SourceSchemaNameQt, '.', @SourceTableNameQt
    , ' for date range from: ', +CONVERT(NVARCHAR(64), @FromDate), ' to: ', CONVERT(NVARCHAR(64), @ToDate)))
    SET @Sql = N'SELECT @_count = COUNT_BIG(*) FROM '+@SourceSchemaNameQt+'.'+@SourceTableNameQt+' WHERE SystemUpdatedDt >= '''+CONVERT(NVARCHAR(64), @FromDate)+''' AND SystemUpdatedDt < '''+CONVERT(NVARCHAR(64), @ToDate)+''';'
END

SET @Parameter = N'@_count varchar(30) OUTPUT';
EXECUTE sp_executesql @Sql, @Parameter, @_count = @Count OUTPUT;

-------------------------------------
END

GO

