USE [ReplicationDB]
GO

SET NOCOUNT ON
SET XACT_ABORT ON
-----------------------------------------------------------------------------------------------
-- setup default file path:
DECLARE @UseDeafultDataFilesPath BIT = 1
DECLARE @ManualDataFilesPath NVARCHAR(256) = NULL --'D:\MSSQL12.MSSQLSERVER\MSSQL\DATA' --NULL --'F:\ENTER THE PATH YOU WANT HERE and set @UseDeafultDataFilesPath = 0'
DECLARE @DBFilesPath NVARCHAR(256)

DECLARE @DeafultDataFilesPath NVARCHAR(256)
IF (@UseDeafultDataFilesPath = 1)
BEGIN
	SELECT TOP 1 @DeafultDataFilesPath = physical_name FROM master.sys.database_files df WHERE df.type = 0
	PRINT '@DeafultDataFilesPath Full: '+@DeafultDataFilesPath
	SET @DeafultDataFilesPath = REVERSE(STUFF(REVERSE(@DeafultDataFilesPath), 1, CHARINDEX('\', REVERSE(@DeafultDataFilesPath)), ''))
	PRINT '@DeafultDataFilesPath Adjusted: '+@DeafultDataFilesPath
    SET @DBFilesPath = @DeafultDataFilesPath+'\'
END

IF (@UseDeafultDataFilesPath = 0 AND @ManualDataFilesPath IS NOT NULL)
BEGIN
	SET @DBFilesPath = @ManualDataFilesPath+'\'
END
PRINT '@DBFilesPath: '+ISNULL(@DBFilesPath, '')
-----------------------------------------------------------------------------------------------


DECLARE @DatabaseName NVARCHAR(256) = N'ReplicationDB'
DECLARE @StartingDate DATETIME2 = '2018-06-01 00:00:00.000'

DECLARE @AlterDatabaseStmt NVARCHAR(MAX) = N'';
DECLARE @FileGroupNm NVARCHAR(256) = N''
DECLARE @AddFileStmt NVARCHAR(MAX) = N''
DECLARE @FileNm NVARCHAR(256) = N''
DECLARE @FileSize NVARCHAR(16) = N'16MB'
DECLARE @FileGrowth NVARCHAR(16) = N'8MB'
DECLARE @FullFilePath NVARCHAR(1024) = N''
DECLARE @FileExists INT = 1


DECLARE @i DATETIME2 = @StartingDate;
WHILE @i < DATEADD(MONTH, 6, GETDATE())  -- we add 6 months because that is the limitation of NVARCHAR(MAX) that comes up later in creating PartitionScheme and PF
    BEGIN  
        
        SET @FileGroupNm = LEFT(CONVERT(NVARCHAR, @i , 112), 6) -- we want the YYYYMM filegroup format
 
        IF NOT EXISTS (SELECT [name] FROM sys.filegroups WHERE name = @FileGroupNm) 
               BEGIN
                     -- add filegroup:
                     SET @AlterDatabaseStmt = N'ALTER DATABASE ['+@DatabaseName+'] ADD FILEGROUP ['+@FileGroupNm+'];'
                     EXEC sp_executesql @AlterDatabaseStmt; 
                     PRINT(@AlterDatabaseStmt)

                     -- check if file exists:
                     SELECT @FullFilePath = @DBFilesPath+@DatabaseName+'_'+@FileGroupNm+'.ndf'
                     EXEC master.dbo.xp_fileexist @FullFilePath, @FileExists OUTPUT

                     IF (@FileExists = 0)
                     BEGIN
                            -- add file to filegroup:
                            SET @AddFileStmt = 'ALTER DATABASE ['+@DatabaseName+']
                            ADD FILE
                            (
                                NAME = FG_'+@FileGroupNm+',
                                FILENAME = '''+@FullFilePath+''',
                                SIZE = '+@FileSize+',
                                FILEGROWTH = '+@FileGrowth+'
                            ) TO FILEGROUP ['+@FileGroupNm+'];'
                            EXEC sp_executesql @AddFileStmt; 
                            PRINT(@AddFileStmt)
                     END
               END  
        SET @i = DATEADD(MONTH, 1, @i);
    END  

 
