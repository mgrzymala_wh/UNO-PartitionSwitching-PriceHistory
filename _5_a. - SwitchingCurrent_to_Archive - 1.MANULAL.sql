USE [BusinessSemanticLayer]
GO

SET XACT_ABORT ON
SET NOCOUNT ON

DECLARE @_LowPartitionBoundary DATETIME
DECLARE @_HighPartitionBoundary DATETIME
DECLARE @_PartitionNumber INT
DECLARE @_BoundariesDayDifference INT
DECLARE @_MinAllowedBoundaryDifference INT = 200
DECLARE @Sql VARCHAR(1024), @ErrMsg VARCHAR(MAX)
DECLARE @TableName NVARCHAR(256) = 'PriceHistory_Current'
DECLARE @_Count BIGINT

EXEC [dbo].[usp_GetCountFromSource]
                  @SourceTableName  = @TableName --'PriceHistory_Source'
                , @FromDate         = NULL
                , @ToDate           = NULL
                , @Count            = @_Count OUTPUT
-- 0. if COUNT(*) FROM @TableName = 0 RAISERROR otherwise get oldest non-empty partition number:
IF (@_Count > 0)
BEGIN
    EXEC [dbo].[usp_Get_PartitionBoundaries_OldestNonEmptyPartitionNumber] @_LowPartitionBoundary OUTPUT, @_HighPartitionBoundary OUTPUT, @_PartitionNumber OUTPUT, @_TableName = @TableName
END
ELSE
BEGIN
      SET @ErrMsg = 'Error executing: ' + COALESCE(OBJECT_NAME(@@PROCID), 'Unknown procedure') + CHAR(13)
      SET @ErrMsg += CONCAT('Table: ', @TableName, ' is empty')
      ;THROW 99001, @ErrMsg, 1;
END

-- 1. check if the difference betwen Lowest Non-empty and Highest Partition-boundaries is within the min. allowed range (and if it's not raise an error):
SELECT @_BoundariesDayDifference = DATEDIFF(DAY, @_LowPartitionBoundary, @_HighPartitionBoundary)
IF (@_BoundariesDayDifference IS NULL OR @_PartitionNumber IS NULL)
BEGIN
      SET @ErrMsg = 'Error executing: ' + COALESCE(OBJECT_NAME(@@PROCID), 'Unknown procedure') + CHAR(13)
      SET @ErrMsg += CONCAT('OldestNonEmptyPartitionNumber or @_BoundariesDayDifference is NULL, make sure that table: ', @TableName
                        , ' contains values in the date range from: '+CONVERT(NVARCHAR(32), @_LowPartitionBoundary)+' to: '+CONVERT(NVARCHAR(32), @_HighPartitionBoundary))
      ;THROW 99001, @ErrMsg, 1;
END
IF (@_BoundariesDayDifference < @_MinAllowedBoundaryDifference)
BEGIN
      SET @ErrMsg = 'Error executing: ' + COALESCE(OBJECT_NAME(@@PROCID), 'Unknown procedure') + CHAR(13) + '
      The difference ('+CONVERT(NVARCHAR(32), @_BoundariesDayDifference)+' days) betwen the Lowest Non-empty partition-boundary ('+CONVERT(NVARCHAR(32), @_LowPartitionBoundary)+') 
      and Highest partition-boundary ('+CONVERT(NVARCHAR(32), @_HighPartitionBoundary)+') is smaller than the min. allowed range: '+CONVERT(NVARCHAR(32), @_MinAllowedBoundaryDifference)+ CHAR(13)
      RAISERROR ( @ErrMsg 
	             ,20 -- @ErrorSeverity -- override to 20 to break execution
	             ,-1
	            ) WITH LOG;
END
ELSE
BEGIN
    PRINT('The difference ('+CONVERT(NVARCHAR(32), @_BoundariesDayDifference)+' days) betwen the Lowest Non-empty partition boundary ('+CONVERT(NVARCHAR(32), @_LowPartitionBoundary)+') 
      and the Highest partition boundary ('+CONVERT(NVARCHAR(32), @_HighPartitionBoundary)+') is within the min. allowed range: '+CONVERT(NVARCHAR(32), @_MinAllowedBoundaryDifference))+' days'+ CHAR(13)+'
      Next Partition to be processed: '+CONVERT(VARCHAR(4), @_PartitionNumber)
END

BEGIN TRANSACTION

-- 2. convert the input parameters to VARCHAR to be able to use them in EXEC(@Sql) and MOVE @LowPartitionBoundary 1 DAY AHEAD
DECLARE @LowPartitionBoundary VARCHAR(128) = CONVERT(VARCHAR(128), DATEADD(DAY, 1, @_LowPartitionBoundary))
DECLARE @HighPartitionBoundary VARCHAR(128) = CONVERT(VARCHAR(128), DATEADD(DAY, 1, @_HighPartitionBoundary)) -- this is because the latest records in [PriceHistory_Current] include the last day in the partition range
DECLARE @LowPartitionBoundaryMinusOne VARCHAR(128) =  @_LowPartitionBoundary -- save the @_LowPartitionBoundary without MOVING 1 DAY AHEAD to use in the constraint adjustment (in step 5)
DECLARE @PartitionNumber VARCHAR(4) = CONVERT(VARCHAR(4), @_PartitionNumber)

SELECT @LowPartitionBoundary AS [LowPartitionBoundary], @HighPartitionBoundary AS [HighPartitionBoundary], @PartitionNumber AS [PartitionNumber]

-- 3. verify if [PriceHistory_CurrentToArchive_Staging] table is empty, otherwise raise an error:
IF (SELECT COUNT(*) FROM [PriceHistory_CurrentToArchive_Staging]) > 0
BEGIN
      --TRUNCATE TABLE [PriceHistory_CurrentToArchive_Staging]
      SET @ErrMsg = 'Error executing: ' + COALESCE(OBJECT_NAME(@@PROCID), 'Unknown procedure') + CHAR(13) + '
      There are existing records in table [PriceHistory_CurrentToArchive_Staging] that should not be there, please make sure previous run of partition-switch 
      from [PriceHistory_CurrentToArchive_Staging] to [PriceHistory_Archive] executed correctly' 
      RAISERROR ( @ErrMsg 
	             ,20 -- @ErrorSeverity -- override to 20 to break execution
	             ,-1
	            ) WITH LOG;
END

-- 4. re-create the ROWSTORE INDEX on [PriceHistory_CurrentToArchive_Staging] table since last exec created a COLUMNSTORE INDEX on it, with the same name (in step 8):
CREATE CLUSTERED INDEX [IX_CL_dbo_PriceHistory_CurrentToArchive_Staging_SystemUpdatedDt] ON [dbo].[PriceHistory_CurrentToArchive_Staging]
(
 [SystemUpdatedDt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = ON, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [ps_daily_date]([SystemUpdatedDt])

-- 5. open access to [PriceHistory_CurrentToArchive_Staging] - adjust the [DF_dbo_PriceHistory_CurrentToArchive_Staging_SystemUpdatedDt] constraint on [PriceHistory_CurrentToArchive_Staging]: move the Lowest/Highest-Value-Constraints up to allow newer records to be merged-in
IF EXISTS (SELECT * FROM sys.objects WHERE [name] = 'DF_dbo_PriceHistory_CurrentToArchive_Staging_SystemUpdatedDt')
BEGIN
    PRINT('Step 5: Processing table: [PriceHistory_CurrentToArchive_Staging] dropping constraint [DF_dbo_PriceHistory_CurrentToArchive_Staging_SystemUpdatedDt]')
    ALTER TABLE [dbo].[PriceHistory_CurrentToArchive_Staging] DROP CONSTRAINT [DF_dbo_PriceHistory_CurrentToArchive_Staging_SystemUpdatedDt]
END
SELECT @Sql = 'ALTER TABLE [dbo].[PriceHistory_CurrentToArchive_Staging] WITH CHECK ADD CONSTRAINT [DF_dbo_PriceHistory_CurrentToArchive_Staging_SystemUpdatedDt] CHECK (SystemUpdatedDt >= '''+@LowPartitionBoundaryMinusOne+''' AND SystemUpdatedDt < '''+@LowPartitionBoundary+''')'
PRINT 'Step 5: Opening access to table: [PriceHistory_CurrentToArchive_Staging], adjusting Lowest/Highest-Value-Constraints to: >= '+@LowPartitionBoundaryMinusOne+' < '+@LowPartitionBoundary
EXEC(@Sql)

-- 6. SWITCH PARTITION from [PriceHistory_Current] to [PriceHistory_CurrentToArchive_Staging]:
SELECT @Sql = 'ALTER TABLE dbo.[PriceHistory_Current] SWITCH PARTITION '+@PartitionNumber+' TO dbo.[PriceHistory_CurrentToArchive_Staging] PARTITION '+@PartitionNumber
PRINT 'Step 6: Switching partition: '+@PartitionNumber+' from [PriceHistory_Current] to [PriceHistory_CurrentToArchive_Staging]'
EXEC(@Sql)

-- 7. after switch is done adjust the [DF_dbo_PriceHistory_Current_SystemUpdatedDt] constraint on [PriceHistory_Current]: move the LowestValue-Constraint up to prevent older records from getting inserted there
IF EXISTS (SELECT * FROM sys.objects WHERE [name] = 'DF_dbo_PriceHistory_Current_SystemUpdatedDt' AND [type] = 'C')
BEGIN
    PRINT('Step 7a: Processing table: [PriceHistory_Current] dropping constraint [DF_dbo_PriceHistory_Current_SystemUpdatedDt]')
    ALTER TABLE [dbo].[PriceHistory_Current] DROP CONSTRAINT [DF_dbo_PriceHistory_Current_SystemUpdatedDt] 
END
SELECT @Sql = 'ALTER TABLE [dbo].[PriceHistory_Current] WITH CHECK ADD CONSTRAINT [DF_dbo_PriceHistory_Current_SystemUpdatedDt] CHECK (SystemUpdatedDt >= '''+@LowPartitionBoundary+''' AND SystemUpdatedDt < '''+@HighPartitionBoundary+''')'
PRINT 'Step 7b: Processing table: [PriceHistory_Current], adjusting Lowest/HighestValue-Constraint to: >= '+@LowPartitionBoundary+' < '+@HighPartitionBoundary
EXEC(@Sql)

-- 8. reverse of step 1: re-create the COLUMNSTORE INDEX on [PriceHistory_CurrentToArchive_Staging] table since step 4 created a ROWSTORE INDEX on it with the same name:
CREATE CLUSTERED COLUMNSTORE INDEX [IX_CL_dbo_PriceHistory_CurrentToArchive_Staging_SystemUpdatedDt] ON dbo.[PriceHistory_CurrentToArchive_Staging]
WITH (MAXDOP = 1, DROP_EXISTING = ON, DATA_COMPRESSION = COLUMNSTORE_ARCHIVE)

-- 9. adjust the [DF_dbo_PriceHistory_Archive_SystemUpdatedDt] constraint on [PriceHistory_Archive]: move the HighestValue-Constraint up to allow newer records to be merged-in
IF EXISTS (SELECT * FROM sys.objects WHERE [name] = 'DF_dbo_PriceHistory_Archive_SystemUpdatedDt' AND [type] = 'C')
BEGIN
    PRINT('Step 9a: Processing table: [PriceHistory_Archive] dropping constraint [DF_dbo_PriceHistory_Archive_SystemUpdatedDt]')
    ALTER TABLE [dbo].[PriceHistory_Archive] DROP CONSTRAINT [DF_dbo_PriceHistory_Archive_SystemUpdatedDt] 
END
SELECT @Sql = 'ALTER TABLE [dbo].[PriceHistory_Archive] WITH CHECK ADD CONSTRAINT [DF_dbo_PriceHistory_Archive_SystemUpdatedDt] CHECK (SystemUpdatedDt < '''+@LowPartitionBoundary+''')'
PRINT 'Step 9b: Processing table: [PriceHistory_Archive], adjusting HighestValue-Constraint to: '+@LowPartitionBoundary
EXEC(@Sql)


-- 10. SWITCH PARTITION from [PriceHistory_CurrentToArchive_Staging] to [PriceHistory_Archive]:
SELECT @Sql = 'ALTER TABLE dbo.[PriceHistory_CurrentToArchive_Staging] SWITCH PARTITION '+@PartitionNumber+' TO dbo.[PriceHistory_Archive] PARTITION '+@PartitionNumber
PRINT 'Step 10: Switching partition: '+@PartitionNumber+' from [PriceHistory_CurrentToArchive_Staging] to [PriceHistory_Archive]'
EXEC(@Sql)

-- 11. verify again if [PriceHistory_CurrentToArchive_Staging] table is empty, otherwise raise an error:
IF (SELECT COUNT(*) FROM [PriceHistory_CurrentToArchive_Staging]) > 0
BEGIN
      SET @ErrMsg = 'Error executing: ' + COALESCE(OBJECT_NAME(@@PROCID), 'Unknown procedure') + CHAR(13) + '
      After partition-switch from [PriceHistory_CurrentToArchive_Staging] to [PriceHistory_Archive] there are leftover records in table 
      [PriceHistory_CurrentToArchive_Staging] that should not be there, please make sure the partition-switch executed correctly' 
      RAISERROR ( @ErrMsg 
	             ,20 -- @ErrorSeverity -- override to 20 to break execution
	             ,-1
	            ) WITH LOG;
END

-- 12. close access to [PriceHistory_CurrentToArchive_Staging] - adjust the [DF_dbo_PriceHistory_CurrentToArchive_Staging_SystemUpdatedDt] constraint on [PriceHistory_CurrentToArchive_Staging]: even out Lowest/Highest-Value-Constraints to prevent aby records from getting merged-in
IF EXISTS (SELECT * FROM sys.objects WHERE [name] = 'DF_dbo_PriceHistory_CurrentToArchive_Staging_SystemUpdatedDt' AND [type] = 'C')
BEGIN
    PRINT('Step 12a: Processing table: [PriceHistory_CurrentToArchive_Staging] dropping constraint [DF_dbo_PriceHistory_CurrentToArchive_Staging_SystemUpdatedDt]')
    ALTER TABLE [dbo].[PriceHistory_CurrentToArchive_Staging] DROP CONSTRAINT [DF_dbo_PriceHistory_CurrentToArchive_Staging_SystemUpdatedDt]
END
SELECT @Sql = 'ALTER TABLE [dbo].[PriceHistory_CurrentToArchive_Staging] WITH CHECK ADD CONSTRAINT [DF_dbo_PriceHistory_CurrentToArchive_Staging_SystemUpdatedDt] CHECK (SystemUpdatedDt > '''+@LowPartitionBoundary+''' AND SystemUpdatedDt < '''+@LowPartitionBoundary+''')'
PRINT 'Step 12b: Closing access to table: [PriceHistory_CurrentToArchive_Staging], adjusting Lowest/Highest-Value-Constraints to: > '+@LowPartitionBoundary+' < '+@LowPartitionBoundary
EXEC(@Sql)

COMMIT TRANSACTION
