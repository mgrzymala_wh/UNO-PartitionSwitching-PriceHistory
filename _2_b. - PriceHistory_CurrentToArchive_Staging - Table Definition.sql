USE [BusinessSemanticLayer]
GO

/****** Object:  Table [dbo].[PriceHistory]    Script Date: 4/28/2020 6:18:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET XACT_ABORT ON
GO

DROP TABLE IF EXISTS [dbo].[PriceHistory_CurrentToArchive_Staging]

CREATE TABLE [dbo].[PriceHistory_CurrentToArchive_Staging](
	[PriceHistoryId] [BIGINT] NOT NULL,
	[MarketOfferSourceId] [BIGINT] NOT NULL,
	[PriceHistoryFromDt] [DATETIME] NOT NULL,
	[PriceHistoryToDt] [DATETIME] NOT NULL,
	[PriceHistoryOrderId] [BIGINT] NOT NULL,
	[MarketOfferId] [BIGINT] NOT NULL,
	[OddsNumerator] [INT] NULL,
	[OddsDenominator] [INT] NULL,
	[PriceHistoryStatus] [NCHAR](1) NULL,
	[PriceHistoryDisplayedFlag] [NCHAR](1) NULL,
	[PriceHistoryResult] [NCHAR](1) NULL,
	[PriceHistoryPlace] [INT] NULL,
	[CashoutPriceNumerator] [INT] NULL,
	[CashoutPriceDenominator] [INT] NULL,
	[StartPriceNumerator] [INT] NULL,
	[StartPriceDenominator] [INT] NULL,
	[ActionType] [NCHAR](1) NULL,
	[UpdatedByStaffId] [INT] NOT NULL,
	[InformationSourceId] [INT] NOT NULL,
	[SystemCreatedDt] [DATETIME] NOT NULL,
	[SystemUpdatedDt] [DATETIME] NOT NULL
) ON [ps_daily_date]([SystemUpdatedDt])
GO

CREATE CLUSTERED INDEX [IX_CL_dbo_PriceHistory_CurrentToArchive_Staging_SystemUpdatedDt] ON [dbo].[PriceHistory_CurrentToArchive_Staging]
(
	[SystemUpdatedDt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [ps_daily_date]([SystemUpdatedDt])
GO

ALTER TABLE [dbo].[PriceHistory_CurrentToArchive_Staging] ADD CONSTRAINT [PK_dbo_PriceHistory_CurrentToArchive_Staging_PriceHistoryId]  PRIMARY KEY NONCLUSTERED ([PriceHistoryId], [SystemUpdatedDt])
GO

ALTER TABLE [dbo].[PriceHistory_CurrentToArchive_Staging] ADD CONSTRAINT [DF_dbo_PriceHistory_CurrentToArchive_Staging_PriceHistoryToDt]  DEFAULT ('9999-12-31 23:59:59') FOR [PriceHistoryToDt]
GO

ALTER TABLE [dbo].[PriceHistory_CurrentToArchive_Staging] ADD CONSTRAINT [DF_dbo_PriceHistory_CurrentToArchive_Staging_UpdatedByStaffId]  DEFAULT ((1)) FOR [UpdatedByStaffId]
GO

ALTER TABLE [dbo].[PriceHistory_CurrentToArchive_Staging] ADD CONSTRAINT [DF_dbo_PriceHistory_CurrentToArchive_Staging_InformationSourceId]  DEFAULT ((2)) FOR [InformationSourceId]
GO

DECLARE @SystemUpdatedDtMin DATETIME, @SystemUpdatedDtMax DATETIME, @Sql VARCHAR(1024)

SELECT      @SystemUpdatedDtMin = CONVERT(DATETIME, MIN(sprv.value)),
            @SystemUpdatedDtMax = CONVERT(DATETIME, MAX(sprv.value))
FROM        sys.partition_functions AS spf
INNER JOIN  sys.partition_range_values sprv ON sprv.function_id=spf.function_id
WHERE       (spf.name = N'pf_daily_date')

SELECT @Sql = 'ALTER TABLE [dbo].[PriceHistory_CurrentToArchive_Staging] WITH CHECK ADD CONSTRAINT [DF_dbo_PriceHistory_CurrentToArchive_Staging_SystemUpdatedDt] CHECK (SystemUpdatedDt >= '''+CONVERT(NVARCHAR(64), @SystemUpdatedDtMin)+''' AND SystemUpdatedDt < '''+CONVERT(NVARCHAR(64), @SystemUpdatedDtMax)+''')'
PRINT 'Processing table: [PriceHistory_CurrentToArchive_Staging], adjusting Lowest/Highest-Value-Constraints to: >= '+CONVERT(NVARCHAR(64), @SystemUpdatedDtMin)+' < '+CONVERT(NVARCHAR(64), @SystemUpdatedDtMax)
EXEC(@Sql)
GO
